from django.shortcuts import render
from django.views.generic import FormView, RedirectView,TemplateView

from django.http import HttpResponseRedirect
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from rest_framework.response import Response
from rest_framework.views import APIView
from django.contrib.auth import (
        REDIRECT_FIELD_NAME,
        login as auth_login,
        logout as auth_logout,
        authenticate)
from django.core.mail import send_mail
from send.models import email_insert,draft
from django.contrib.auth.mixins import LoginRequiredMixin,UserPassesTestMixin
from django.contrib import messages

# Create your views here.
class LoginView(FormView):
    """
    Provides the ability to login as a user with a username and password
    """
    form_class = AuthenticationForm
    template_name = "login.html"

    def post(self, request):
        username = self.request.POST['username']
        password = self.request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                auth_login(request, user)
                return HttpResponseRedirect('/home')
            else:
                return HttpResponseRedirect('/')
        else:
            return HttpResponseRedirect('/')
        
from django.core.mail import EmailMessage

class HomeView(LoginRequiredMixin,TemplateView):
    login_url='/'
    
    template_name = "home.html"
    def post(self, request, *args, **kwargs):
        if 'btn1' in request.POST:
            name=request.POST.get('email')
            email_insert.objects.create(Name=name)
            return HttpResponseRedirect('/home')
        if 'btn2' in request.POST:
            print("second")
            head=request.POST.get('head')
            subject=request.POST.get('subject')
            a=email_insert.objects.all()
            all_email=[]
            for r in a:
                all_email.append(r.Name)
            print(head)
            print(subject)
            print(all_email)
            #semail = EmailMessage(str(head),str(subject), to=all_email)
            #semail.send()
            draft.objects.create(head=head,subject=subject)
            return HttpResponseRedirect('/home')
    def get_context_data(self, **kwargs): 
        context = kwargs
        context['email']=email_insert.objects.all()
        context['data']=draft.objects.all()
        return context
    
class edit(APIView):
    def get(self, request, *args, **kwargs):
        user_id = kwargs['id']
        email_insert.objects.filter(id=user_id).delete()
        return HttpResponseRedirect('/home')

class drafts(APIView):
    def post(self, request, *args, **kwargs):
        head=request.POST.get('s')
        subject=request.POST.get('a')
        id=request.POST.get('id')
        draft.objects.filter(id=id).update(head=head,subject=subject)
        return Response("success")

class send_all(APIView):
    def post(self, request, *args, **kwargs):
        head=request.POST.get('s')
        subject=request.POST.get('a')
        a=email_insert.objects.all()
        all_email=[]
        for r in a:
            all_email.append(r.Name)
        print(head)
        print(subject)
        print(all_email)
        semail = EmailMessage(str(head),str(subject), to=all_email)
        semail.send()
        return Response("success")